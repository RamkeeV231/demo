Selenium Demo
By Ramkee venkatraman
ramkee.v@gmail.com
214-600-0272

Authors' Note:

***** If possible, I would like to have an online conversation, to discuss in detail about the framework.*****

1. Given the time of 2 days and no solid requiremnts, I have chosen to concentrate more on the framework and design patterns than the actual applications functionalities. 
2. My understanding is, setting up a Robust and Flexible framework has priority compared to automating functionalities.
3. That being said, the framework is nowhere near complete. As One might expect given the time, size of aliexpress site, and requirements, 
its not possible to create an air tight frame work in 2 day.
4. And Mostly, I really apologize as I haven't added comments in my script. Given more time I would have.


****Tools and set up.*****

Build tool- 	Maven
IDE- 		Eclipse oxygen
Reporting - 	ExtentReports 2.41.2, TestNg
Looger - 	Log4j 1.2.17 
Selenium 	version 3.4.0
Data - 		Excel Apache PoI(3.15)
Version COntrol BitBucket

****Designs and Framework****

Object Repository model	:	Page Object Model
Framework		:	Data-Keyword Hybrid

******* Packages and explanations ********

utility Package: 	Contains all the utility classes which is not related to application, like extent report, fluentwait class,screenshothelper
excelData Package: 	Contains 2 important classes related to data from excel. 
			TestDataProvider class - Provides DataProvider methods for our Testcases, by invoking Exceldatamanger to fetch data from excel.
			ExcelDataManger acts the sole class to have access to excel data. All excel related operations goes through this class definitions.
tableDataPackage:	I created these classes to store data from each table in excel to a corresponding table class object. 
			In essence each row of excel sheet is an object of its corresponding class and each column is its variable.

		*****Note: A database with a proper ORM(Hibernate) would provide much better functionality and elminate our need to create above tables.******************

pageActions And pageXpaths    :	By using Page object model, each page has its own xpath class and a corresponding function class. 
			 	This provides us with better control over maintaining xpaths and to reuse functions at our ease.



******** Database Structure******
Note: I have created data in Excel as I would create in Database. Given the time constraint and ease of sharing, I have used Excel here.

Fully Normalized Database.

TestCases table Contains all the testcases with Y/N execution column and data to refrence other tables in the strucutre, in a fully Normalized schema. 
This allows us to add new functionality or new testcases without having to change the entire structure of test data on update.

Eg: If we have more than one pay ment method. 
	1. Each payment method name will be given in main testcase table. (Lets say Paypal)
	2. A corresponding table to paypal payment will have TestcaseID and PayPal account id.
	3. PaypalDetails will have Account id and details pertaining to that account.

	This enables us to add payment types at ease, as well as use existing account details accross the board. 

********* Framework **********
Hybrid framework: 	1 The real advantage in using hybrid framework is its reusability and maintainability. 
			2 A new functionality in exisitng testflow could be added to affect all similar cases in a single go and vice versa.
			3 New test flows can be added easily as well, without disturbing the data schema or the design structure of framework.
			4 Every keyword can be further split into subtypes to add more functionality,by creating tables corresponding to keywords with more data and keyword. 

	



			
