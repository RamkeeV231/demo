package sleniumDemo;

import static org.testng.Assert.fail;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;

import com.relevantcodes.extentreports.*;

import ExcelData.ExcelDataManager;
import ExcelData.TestDataProvider;

import pageActions.*;
import tableData.*;
import utility.*;


public class SampleTest {

	protected ExtentReports extent;
	protected ExtentTest test;
	protected String filePath;
	protected Logger logger;
	protected WebDriver driver;
	protected WaitManager fluentWait;
	protected WebDriverWait wait;
	ExcelDataManager excel;

	@BeforeTest
	public void setup() {
		logger = Logger.getLogger(SampleTest.class);

		// configure log4j properties file
		PropertyConfigurator.configure(System.getProperty("user.dir") + "\\Log4j.properties");
		filePath = System.getProperty("user.dir") + "\\ExtentReport\\Result"+System.currentTimeMillis()+".html";
		System.out.println(filePath);
		extent = ExtentManager.getReporter(filePath);
		excel = new ExcelDataManager();

	}

	@BeforeMethod
	public void openSite() {
		try {
			System.setProperty("webdriver.gecko.driver", "B:\\eclipse\\gecko\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			wait = new WebDriverWait(driver, 20);
			fluentWait = new WaitManager(driver);
		} catch (Exception e) {
			logger.error(e);
			Assert.fail();
		}

	}

	@Test(dataProvider = "checkoutMethodDataProvider", dataProviderClass = TestDataProvider.class)
	public void checkout(TestCasesData testCase) {
		
		
		
		test = extent.startTest(testCase.testcaseID);
		test.log(LogStatus.INFO, testCase.testcaseDescription);
		driver.get("http://www.aliexpress.com/");
		wait.until(ExpectedConditions.titleContains(
				"AliExpress.com - Online Shopping for Electronics, Fashion, Home & Garden, Toys & Sports"));
		test.log(LogStatus.PASS, "Home page is loaded");
		
		
		HomePageActions home = new HomePageActions(driver, test);
		Assert.assertTrue(home.closePromotion(driver), "close promotion failed in HomePageActions class");
		
		Assert.assertTrue(home.searchItem(driver, testCase.productId), "Search item failed in HomePageActions class");
		
		ProductDetails product= excel.getProductDetails(testCase.productId);
		

		wait.until(ExpectedConditions.titleContains(product.productName));
		test.log(LogStatus.PASS, "Product page is loaded");
		
		Assert.assertTrue(home.closePromotion(driver), "close promotion failed in HomePageActions class");
		
		
		PrdPageActions prd= new PrdPageActions(driver, test);
		Assert.assertTrue(prd.selectBundleBtn(driver,product.bundleIndex), "Select bundle failed");
		Assert.assertTrue(prd.selectColorBtn(driver,product.colorIndex),"Select color failed");
		Assert.assertTrue(prd.enterQty(driver, product.qty),"Select qty failed");
		Assert.assertTrue(prd.clickAddToCartBtn(driver),"click add to cart failed");
		Assert.assertTrue(prd.clickViewShoppingcartBtn(driver),"click view shopping cart failed");
		ExtentManager.addScreenshot(driver, test);
		driver.quit();

	}
	
	@Test(dataProvider = "signinMethodDataProvider", dataProviderClass = TestDataProvider.class)
	public void signIn(TestCasesData testCase) {
		test = extent.startTest(testCase.testcaseID);
		test.log(LogStatus.INFO, testCase.testcaseDescription);
		driver.get("http://www.aliexpress.com/");
		wait.until(ExpectedConditions.titleContains(
				"AliExpress.com - Online Shopping for Electronics, Fashion, Home & Garden, Toys & Sports"));
		test.log(LogStatus.PASS, "Home page is loaded");
		
		
		HomePageActions home = new HomePageActions(driver, test);
		Assert.assertTrue(home.closePromotion(driver), "close promotion failed in HomePageActions class");
		Assert.assertTrue(home.clickSignin(driver), "signin button not clickable");
		wait.until(ExpectedConditions.titleContains("Buy Products Online from China Wholesalers at Aliexpress.com"));
		test.log(LogStatus.PASS, "Sign in page is loaded");
		
		LoginPageActions login = new LoginPageActions(driver, test);
		UserDetails user= excel.getUserDetails(testCase.userID);
		Assert.assertTrue(login.enterID(driver, user.userID),"enter id failed");
		Assert.assertTrue(login.enterPassword(driver, user.userPassword),"enter password failed");
		Assert.assertTrue(login.clickSigninBtn(driver),"enter signin failed");
		if(user.userAuthentication.compareTo("Yes")==0)
		{
		wait.until(ExpectedConditions.titleContains(
				"AliExpress.com - Online Shopping for Electronics, Fashion, Home & Garden, Toys & Sports"));
		test.log(LogStatus.PASS, "Home page is loaded");
		}
		else
			Assert.assertEquals(login.getErrorText(driver),"Your account name or password is incorrect.","error messgae failed");	
		ExtentManager.addScreenshot(driver, test);
		driver.quit();

	}

	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, result.getThrowable());
		}
		ExtentManager.addScreenshot(driver, test);
		extent.endTest(test);
		
		driver.quit();

	}

	@AfterTest
	public void endTest() {
		extent.flush();
		extent.close();
	}

}
