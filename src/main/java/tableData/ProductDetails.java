package tableData;

public class ProductDetails {
	public String productID;
	public String productName;
	public String qty;
	public String bundleIndex;
	public String colorIndex;
	public ProductDetails(String productID, String productName, String qty, String bundleIndex, String colorIndex) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.qty = qty;
		this.bundleIndex = bundleIndex;
		this.colorIndex = colorIndex;
	}

}
