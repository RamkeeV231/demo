package tableData;

public class UserDetails {
public String userID;
public String userName;
public String userPassword;
public String userAuthentication;
public UserDetails(String userID, String userName, String userPassword, String userAuthentication) {
	super();
	this.userID = userID;
	this.userName = userName;
	this.userPassword = userPassword;
	this.userAuthentication = userAuthentication;
}

}
