package tableData;

public class TestCasesData {
public String testcaseID;
public String testcaseDescription;
public String testcaseType;
public String userID;
public String paymentMethod;
public String productId;



public TestCasesData(String testcaseID, String testcaseDescription, String testcaseType, String userID,
		String paymentMethod, String productId) {
	super();
	this.testcaseID = testcaseID;
	this.testcaseDescription = testcaseDescription;
	this.testcaseType = testcaseType;
	this.userID = userID;
	this.paymentMethod = paymentMethod;
	this.productId = productId;
}



public  TestCasesData()
{
	this.testcaseID="sample1";
	
	this.productId="32801473575";
}


}
