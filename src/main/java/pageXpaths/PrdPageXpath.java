package pageXpaths;

import org.openqa.selenium.By;

public class PrdPageXpath {
private By prdTitleTxt= new By.ByXPath(".//*[@id='j-product-detail-bd']//h1[@class ='product-name']");
private By prdQtyTxt= new By.ByXPath(".//*[@id='j-p-quantity-input']");
private By prdBundleBtn;
private By prdColorBtn;
private By addToCartBtn= new By.ByXPath(".//*[@id='j-add-cart-btn']");
private By buyNowBtn= new By.ByXPath(".//*[@id='j-buy-now-btn']");

private By viewShoppingcartBtn = new By.ByXPath("//a[contains(., 'View Shopping')]");
private By continueShoppingBtn = new By.ByXPath("//button[contains(., 'Continue Shopping')]");
public By getPrdTitleTxt() {
	return prdTitleTxt;
}
public By getPrdQtyTxt() {
	return prdQtyTxt;
}
public By getPrdBundleBtn(String i) {
	
	String xpath= ".//dt[contains(.,'Bundle')]/following:: li["+i+"]";
	prdBundleBtn= new By.ByXPath(xpath);
	
	return prdBundleBtn;
}
public By getPrdColorBtn(String i) {
	
	String xpath= ".//dt[contains(.,'Color')]/following:: li["+i+"]";
	prdColorBtn= new By.ByXPath(xpath);
	return prdColorBtn;
}
public By getAddToCartBtn() {
	return addToCartBtn;
}
public By getBuyNowBtn() {
	return buyNowBtn;
}
public By getViewShoppingcartBtn() {
	return viewShoppingcartBtn;
}
public By getContinueShoppingBtn() {
	return continueShoppingBtn;
}



}
