package pageXpaths;

import org.openqa.selenium.By;

public class HomePageXpath {
	
	private By promotionCloseBtn= new By.ByXPath(".//a[@class='close-layer']");
	private By searchBar= new By.ByXPath(".//*[@id='search-key']");
	private By signinLnk= new By.ByXPath(".//*[@id='nav-user-account']//a[@data-role= 'sign-link']");
	private By wishlistLnk= new  By.ByXPath(".//span[contains(., 'Wish List')]");
	private By cartLnk= new  By.ByXPath(".//span[contains(., 'Cart')]");
	private By accountName= new By.ByXPath(".//*[@id='nav-user-account']//span[@class=\"account-name\"]/b");
	private By signout= new By.ByXPath(".//a[contains(.,'Sign Out')]");
	
	public By getSignout() {
		return signout;
	}
	public By getAccountName() {
		return accountName;
	}
	public By getPromotionCloseBtn() {
		return promotionCloseBtn;
	}
	public By getSearchBar() {
		return searchBar;
	}
	public By getSigninLnk() {
		return signinLnk;
	}
	public By getWishlistLnk() {
		return wishlistLnk;
	}
	public By getCartLnk() {
		return cartLnk;
	}
	
	

}
