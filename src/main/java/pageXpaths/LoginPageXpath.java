package pageXpaths;

import org.openqa.selenium.By;

public class LoginPageXpath {
	private By loginIdTxt= new By.ByXPath(".//input[@name='loginId']");
	private By loginPassowrdTxt= new By.ByXPath(".//input[@id='fm-login-password']");
	private By signInBtn= new By.ByXPath(".//*[@id='fm-login-submit']");
	private By errorMessage= new By.ByXPath(".//*[@id='login-error']/span[@class='notice-descript']");
	private String frameid="alibaba-login-box";
	public By getLoginIdTxt() {
		return loginIdTxt;
	}
	public By getLoginPassowrdTxt() {
		return loginPassowrdTxt;
	}
	public By getSignInBtn() {
		return signInBtn;
	}
	public By getErrorMessage() {
		return errorMessage;
	}
	public String getFrameid() {
		return frameid;
	}
	
}
