package utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;

public class WaitManager {
	 Wait<WebDriver> wait;
	 WebDriver driver;
	public  WaitManager(WebDriver driver)
	{
		this.driver=driver;
	  wait = new FluentWait<WebDriver>(driver)       
			  .withTimeout(20, TimeUnit.SECONDS)    
			  .pollingEvery(5, TimeUnit.SECONDS)    
			  .ignoring(NoSuchElementException.class); 
			    
			  
			    
	}
	public boolean isVisible(By xpath)
	{
		try
		{
		  WebElement aboutMe= wait.until(new Function<WebDriver, WebElement>() {       
			  public WebElement apply(WebDriver driver) { 
			  return driver.findElement(xpath);     
			   }  
			  });
		  return true;
		}
		catch(Exception e)
		{
		return false;
		}
		
	}
}
