package utility;


import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import sleniumDemo.SampleTest;

public class ExtentManager {
private static ExtentReports extent;
protected static Logger logger;
    public synchronized static ExtentReports getReporter(String filePath) {
    	logger=Logger.getLogger(ExtentManager.class);
        
   	 	logger.info("extent started");
        // configure log4j properties file
        PropertyConfigurator.configure(System.getProperty("user.dir")+"\\Log4j.properties");
        if (extent == null) {
            extent = new ExtentReports(filePath, true);
        	
            extent
                .addSystemInfo("Host Name", "Ramkee")
                .addSystemInfo("Environment", "QA");
            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extentConfig.xml"));
        }
        
        return extent;
    }
    public static void addScreenshot(WebDriver driver, ExtentTest test)
    {
    	String screenshot_path=ScreenShotHelper.captureScreenShot(driver);
    	String image= test.addScreenCapture(screenshot_path);
    	test.log(LogStatus.INFO, "Title verification", image);
    }
}
