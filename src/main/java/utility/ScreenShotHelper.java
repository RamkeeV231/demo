package utility;

import java.io.*;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotHelper {


	public static String captureScreenShot(WebDriver ldriver){
		String filePath=System.getProperty("user.dir")+"\\Screenshot\\"+System.currentTimeMillis()+".png";
		// Take screenshot and store as a file format
		TakesScreenshot ts=  ((TakesScreenshot)ldriver);
		File src=ts.getScreenshotAs(OutputType.FILE);
		try {
			// now copy the  screenshot to desired location using copyFile method

			FileUtils.copyFile(src, new File(filePath));
			return filePath;
		}

		catch (IOException e)

		{

			System.out.println(e.getMessage());
			return null;

		}

	}
}
