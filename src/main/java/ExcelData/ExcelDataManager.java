package ExcelData;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import tableData.ProductDetails;
import tableData.TestCasesData;
import tableData.UserDetails;
import utility.ExtentManager;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.gargoylesoftware.htmlunit.javascript.host.Iterator;

public class ExcelDataManager {
	Logger logger;
	FileInputStream fis;
	XSSFWorkbook wb;

	public ExcelDataManager() {
		try {
			logger = Logger.getLogger(ExcelDataManager.class);
			// Specify the path of file
			File src = new File(System.getProperty("user.dir") + "\\ExcelData\\Demo.xlsx");

			// load file
			fis = new FileInputStream(src);

			// Load workbook
			wb = new XSSFWorkbook(fis);
		} catch (Exception E) {
			logger.error(E);
		}
	}

	public List<TestCasesData> getTestCases(String testCaseType) {
		if (testCaseType == null)
			return null;
		try {
			List<TestCasesData> testcases = new ArrayList<TestCasesData>();
			XSSFSheet sh1 = wb.getSheet("TestCases");
			for (Row row : sh1) {
				if ((testCaseType.compareTo(row.getCell(2).getStringCellValue()) == 0)
						&& (row.getCell(6).getStringCellValue().compareTo("Y")) == 0) {
					TestCasesData temp = new TestCasesData(row.getCell(0).getStringCellValue(),
							row.getCell(1).getStringCellValue(), row.getCell(2).getStringCellValue(),
							row.getCell(3).getStringCellValue(), row.getCell(4).getStringCellValue(),
							row.getCell(5).getStringCellValue());
					testcases.add(temp);

				}

			}

			return testcases;
		} catch (Exception e) {

			logger.error(e);
			return null;
		}
	}

	public UserDetails getUserDetails(String UserID) {
		if (UserID == null)
			return null;
		try {
			XSSFSheet sh1 = wb.getSheet("UserDetails");
			UserDetails user = null;
			;
			for (Row row : sh1) {
				{
					if (UserID.compareTo(row.getCell(0).getStringCellValue()) == 0) {
						user = new UserDetails(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(),
								row.getCell(2).getStringCellValue(), row.getCell(3).getStringCellValue());
						break;
					}
				}
			}
			return user;
		} catch (Exception e) {

			logger.error(e);
			return null;
		}
	}
	public ProductDetails getProductDetails(String ProductID) {
		if (ProductID == null)
			return null;
		try {
			XSSFSheet sh1 = wb.getSheet("ProductDetails");
			ProductDetails Product = null;
			DataFormatter fmt = new DataFormatter();

			for (Row row : sh1) {
				{
					
					if (ProductID.compareTo(row.getCell(0).getStringCellValue()) == 0) {
						
						Product = new ProductDetails(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue(),
								fmt.formatCellValue(row.getCell(2)),fmt.formatCellValue(row.getCell(3)),fmt.formatCellValue(row.getCell(4)));
						
						break;
					}
				}
			}
			return Product;
		} catch (Exception e) {

			logger.error(e);
			return null;
		}
	}
}
