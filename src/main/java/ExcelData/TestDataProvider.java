package ExcelData;

import java.util.*;

import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;

import pageActions.PrdPageActions;
import tableData.TestCasesData;

public class TestDataProvider {
	static Logger logger;

	@DataProvider(name = "checkoutMethodDataProvider")
	public static TestCasesData[] checkout() {
		logger = Logger.getLogger(TestDataProvider.class);
		try {

			ExcelDataManager excel = new ExcelDataManager();
			List<TestCasesData> testcasesList = excel.getTestCases("Checkout");
			TestCasesData[] TestCases = new TestCasesData[testcasesList.size()];

			int i = 0;
			for (TestCasesData t : testcasesList) {
				TestCases[i] = t;
				i++;
			}

			return TestCases;
		} catch (Exception e) {
			logger.error(e);
			return null;
		}
	}

	@DataProvider(name = "signinMethodDataProvider")
	public static TestCasesData[] Signin() {

		logger = Logger.getLogger(TestDataProvider.class);
		try {
			ExcelDataManager excel = new ExcelDataManager();
			List<TestCasesData> testcasesList = excel.getTestCases("Signin");
			TestCasesData[] TestCases = new TestCasesData[testcasesList.size()];
			int i = 0;
			for (TestCasesData t : testcasesList) {
				TestCases[i] = t;
				i++;
			}

			return TestCases;
		}

		catch (Exception e) {
			logger.error(e);
			return null;
		}
	}
}
