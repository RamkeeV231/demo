package pageActions;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pageXpaths.HomePageXpath;
import utility.ExtentManager;

public class HomePageActions {
	protected HomePageXpath xpath;
	protected WebDriverWait wait ;
	protected  Logger logger;
	protected ExtentTest test;
public HomePageActions(WebDriver driver, ExtentTest test)
{
	this.test= test;
	wait=  new WebDriverWait(driver, 30);
	logger=Logger.getLogger(HomePageActions.class);
	xpath= new HomePageXpath();
}
public boolean  closePromotion(WebDriver driver)
{
	try
	{
	
	wait.until(ExpectedConditions.elementToBeClickable(xpath.getPromotionCloseBtn()));
	driver.findElement(xpath.getPromotionCloseBtn()).click();
	test.log(LogStatus.PASS, "Promotion closed");
	Thread.sleep(2000);
	return true;
	}
	catch(Exception e)
	{
		logger.error(e);
		test.log(LogStatus.FAIL, e);
		return false;
	}
}

public boolean  clickSignin(WebDriver driver)
{
	try
	{
	
	wait.until(ExpectedConditions.elementToBeClickable(xpath.getSigninLnk()));
	driver.findElement(xpath.getSigninLnk()).click();
	test.log(LogStatus.PASS, "Sign in clicked");
	return true;
	}
	catch(Exception e)
	{
		logger.error(e);
		test.log(LogStatus.FAIL, e);
		return false;
	}
}

public boolean  clickCart(WebDriver driver)
{
	try
	{
	
	wait.until(ExpectedConditions.elementToBeClickable(xpath.getCartLnk()));
	driver.findElement(xpath.getCartLnk()).click();
	test.log(LogStatus.PASS, "Cart button clicked");
	return true;
	}
	catch(Exception e)
	{
		logger.error(e);
		test.log(LogStatus.FAIL, e);
		return false;
	}
}

public boolean searchItem(WebDriver driver, String productId)

{
	try {
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getSearchBar()));
		driver.findElement(xpath.getSearchBar()).sendKeys(productId);
		driver.findElement(xpath.getSearchBar()).sendKeys(Keys.RETURN);
		test.log(LogStatus.INFO, "Search Text Entered");
		return true;
	} catch (Exception e) {
		logger.error(e);
		test.log(LogStatus.FAIL, e);
		return false;
	}
}
}
