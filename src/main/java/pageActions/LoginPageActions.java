package pageActions;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pageXpaths.*;

public class LoginPageActions {
	protected LoginPageXpath xpath;
	protected WebDriverWait wait ;
	protected  Logger logger;
	protected ExtentTest test;
	public LoginPageActions(WebDriver driver, ExtentTest test)
	{
		this.test= test;
		wait=  new WebDriverWait(driver, 20);
		logger=Logger.getLogger(LoginPageActions.class);
		xpath= new LoginPageXpath();
	}
	
	public boolean enterID(WebDriver driver, String loginId)

	{
		try {
			
			driver.switchTo().frame(xpath.getFrameid());
			wait.until(ExpectedConditions.visibilityOfElementLocated(xpath.getLoginIdTxt()));
			driver.findElement(xpath.getLoginIdTxt()).sendKeys(loginId);				
			driver.switchTo().defaultContent();
		
			test.log(LogStatus.INFO, "Login ID Entered");
			return true;
		} catch (Exception e) {
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	public boolean enterPassword(WebDriver driver, String Password)

	{
		try {
			
			driver.switchTo().frame(xpath.getFrameid());
			wait.until(ExpectedConditions.visibilityOfElementLocated(xpath.getLoginPassowrdTxt()));
			driver.findElement(xpath.getLoginPassowrdTxt()).sendKeys(Password);
			driver.switchTo().defaultContent();
			test.log(LogStatus.INFO, "Password Entered");
			return true;
		} catch (Exception e) {
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	public boolean  clickSigninBtn(WebDriver driver)
	{
		try
		{
		driver.switchTo().frame(xpath.getFrameid());
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getSignInBtn()));
		driver.findElement(xpath.getSignInBtn()).click();
		driver.switchTo().defaultContent();
		test.log(LogStatus.PASS, "Sign In button clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	public String getErrorText(WebDriver driver)
	{
		try
		{
			driver.switchTo().frame(xpath.getFrameid());
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(xpath.getErrorMessage())));
			String errorText= driver.findElement(xpath.getSignInBtn()).getText();
			/*if(errorText.compareTo("Your account name or password is incorrect.")==0)
			{test.log(LogStatus.PASS, "Sign In button clicked");
			return true;
			}
			else
			{
				logger.error("Error message mismatch");
				test.log(LogStatus.FAIL, "Error message mismatch");
				return false;
			}*/
			driver.switchTo().defaultContent();
			return errorText;	
		}
		
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return e.toString();
		}
	}
}
