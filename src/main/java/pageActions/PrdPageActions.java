package pageActions;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import pageXpaths.*;

public class PrdPageActions {
	protected PrdPageXpath xpath;
	protected WebDriverWait wait ;
	protected  Logger logger;
	protected ExtentTest test;
	public PrdPageActions(WebDriver driver, ExtentTest test)
	{
		this.test= test;
		wait=  new WebDriverWait(driver, 20);
		logger=Logger.getLogger(PrdPageActions.class);
		xpath= new PrdPageXpath();
	}
	
	
	public boolean  selectBundleBtn(WebDriver driver, String i)
	{
		try
		{
			//Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getPrdBundleBtn(i)));
		driver.findElement(xpath.getPrdBundleBtn(i)).click();
		
		test.log(LogStatus.PASS, "Bundle clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	public boolean  selectColorBtn(WebDriver driver, String i)
	{
		try
		{
		
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getPrdColorBtn(i)));
		driver.findElement(xpath.getPrdColorBtn(i)).click();
		test.log(LogStatus.PASS, "Color clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	public boolean enterQty(WebDriver driver, String qty)

	{
		try {
			wait.until(ExpectedConditions.elementToBeClickable(xpath.getPrdQtyTxt()));
			
			driver.findElement(xpath.getPrdQtyTxt()).sendKeys(Keys.chord(Keys.CONTROL, "a"), "55");;
			driver.findElement(xpath.getPrdQtyTxt()).sendKeys(qty);
			
			test.log(LogStatus.INFO, "quantity Entered");
			return true;
		} catch (Exception e) {
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	public boolean  clickAddToCartBtn(WebDriver driver)
	{
		try
		{
		
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getAddToCartBtn()));
		driver.findElement(xpath.getAddToCartBtn()).click();
		test.log(LogStatus.PASS, "AddtoCart clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	
	public boolean  clickContinueShoppingBtn(WebDriver driver)
	{
		try
		{
		
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getContinueShoppingBtn()));
		driver.findElement(xpath.getContinueShoppingBtn()).click();
		test.log(LogStatus.PASS, "ContinueShopping clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	
	public boolean  clickViewShoppingcartBtn(WebDriver driver)
	{
		try
		{
		
		wait.until(ExpectedConditions.elementToBeClickable(xpath.getViewShoppingcartBtn()));
		driver.findElement(xpath.getViewShoppingcartBtn()).click();
		test.log(LogStatus.PASS, "ViewShoppingcart clicked");
		return true;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return false;
		}
	}
	
	public String getPrdTitle(WebDriver driver)
	{
		try
		{

			wait.until(ExpectedConditions.visibilityOf(driver.findElement(xpath.getPrdTitleTxt())));
			String errorText= driver.findElement(xpath.getPrdTitleTxt()).getText();
			return errorText;
		}
		catch(Exception e)
		{
			logger.error(e);
			test.log(LogStatus.FAIL, e);
			return e.toString();
		}
	}
}
